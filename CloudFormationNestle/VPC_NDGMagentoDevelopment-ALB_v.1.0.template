{
  "AWSTemplateFormatVersion" : "2017-10-31",

  "Description" : "Creación VPC para el proyecto Nestlé. Entorno: Magento Development. Componentes: 1 VPC, 1 Internet Gateway, 1 Nat Gateway, 2 Subnets Públicas, 2 Subnets Privadas, 2 ALB Públicos, 1 ALB Privado",

  "Mappings": {
    "Networking": {
      "VPC": {
        "CIDR": "10.44.0.0/16"
      },
      "public-az1": {
        "CIDR": "10.44.0.0/24"
      },
      "public-az2": {
        "CIDR": "10.44.1.0/24"
      },
      "private-az1": {
        "CIDR": "10.44.32.0/21"
      },
      "private-az2": {
        "CIDR": "10.44.40.0/21"
      }
    },

    "RegionMap" : {
      "us-east-1"      : { "AMI" : "ami-7f418316" },
      "us-west-1"      : { "AMI" : "ami-951945d0" },
      "us-west-2"      : { "AMI" : "ami-16fd7026" },
      "eu-west-1"      : { "AMI" : "ami-24506250" },
      "sa-east-1"      : { "AMI" : "ami-3e3be423" },
      "ap-southeast-1" : { "AMI" : "ami-74dda626" },
      "ap-southeast-2" : { "AMI" : "ami-b3990e89" },
      "ap-northeast-1" : { "AMI" : "ami-dcfa4edd" }
    }
},
  
  "Resources" : {

    "VPC" : {
      "Type" : "AWS::EC2::VPC",
      "Properties" : {
        "CidrBlock" : { "Fn::FindInMap": [ "Networking", "VPC", "CIDR" ] },
        "EnableDnsHostnames" : "true",
        "InstanceTenancy" : "default",
        "Tags" : [ {"Key" : "Application", "Value" : { "Ref" : "AWS::StackId"} }, {"Key" : "Name", "Value" : { "Ref" : "NDG Magento Development"} } ]
      }
    },

    "public-az1" : {  
      "Type" : "AWS::EC2::Subnet",
      "Properties" : {
        "AvailabilityZone" : "eu-west-1a",
        "VpcId" : { "Ref" : "VPC" },
        "CidrBlock" : { "Fn::FindInMap": [ "Networking", "public-az1", "CIDR" ] },
        "Tags" : [ {"Key" : "Application", "Value" : { "Ref" : "AWS::StackId"} } ]
      }
    },
    
    "public-az2" : {  
      "Type" : "AWS::EC2::Subnet",
      "Properties" : {
        "AvailabilityZone" : "eu-west-1b",
        "VpcId" : { "Ref" : "VPC" },
        "CidrBlock" : { "Fn::FindInMap": [ "Networking", "public-az2", "CIDR" ] },
        "Tags" : [ {"Key" : "Application", "Value" : { "Ref" : "AWS::StackId"} } ]
      }
    },
    
    "private-az1" : {  
      "Type" : "AWS::EC2::Subnet",
      "Properties" : {
        "AvailabilityZone" : "eu-west-1a",
        "VpcId" : { "Ref" : "VPC" },
        "CidrBlock" : { "Fn::FindInMap": [ "Networking", "private-az1", "CIDR" ] },
        "Tags" : [ {"Key" : "Application", "Value" : { "Ref" : "AWS::StackId"} } ]
      }
    },
    
    "private-az2" : {  
      "Type" : "AWS::EC2::Subnet",
      "Properties" : {
        "AvailabilityZone" : "eu-west-1b",
        "VpcId" : { "Ref" : "VPC" },
        "CidrBlock" : { "Fn::FindInMap": [ "Networking", "private-az2", "CIDR" ] },
        "Tags" : [ {"Key" : "Application", "Value" : { "Ref" : "AWS::StackId"} } ]
      }
    },
    
    "InternetGateway" : {
      "Type" : "AWS::EC2::InternetGateway",
      "Properties" : {
        "Tags" : [ {"Key" : "Application", "Value" : { "Ref" : "AWS::StackId"} } ]
      }
    },

    "AttachGateway" : {
       "Type" : "AWS::EC2::VPCGatewayAttachment",
       "Properties" : {
         "VpcId" : { "Ref" : "VPC" },
         "InternetGatewayId" : { "Ref" : "InternetGateway" }
       }
    },

    "RouteTableIGW" : {
      "Type" : "AWS::EC2::RouteTable",
      "Properties" : {
        "VpcId" : {"Ref" : "VPC"},
        "Tags" : [ {"Key" : "Application", "Value" : { "Ref" : "AWS::StackId"} } ]
      }
    },
    
    "RouteIGW" : {
      "Type" : "AWS::EC2::Route",
      "DependsOn" : "AttachGateway",
      "Properties" : {
        "RouteTableId" : { "Ref" : "RouteTableIGW" },
        "DestinationCidrBlock" : "0.0.0.0/0",
        "GatewayId" : { "Ref" : "InternetGateway" }
      }
    },

    "SubnetRouteTableAssociationIGW1" : {
      "Type" : "AWS::EC2::SubnetRouteTableAssociation",
      "Properties" : {
        "SubnetId" : { "Ref" : "public-az1" },
        "RouteTableId" : { "Ref" : "RouteTableIGW" }
      }
    },

    "SubnetRouteTableAssociationIGW2" : {
      "Type" : "AWS::EC2::SubnetRouteTableAssociation",
      "Properties" : {
        "SubnetId" : { "Ref" : "public-az2" },
        "RouteTableId" : { "Ref" : "RouteTableIGW" }
      }
    },

    "NATGateway" : {
      "DependsOn" : "AttachGateway",
      "Type" : "AWS::EC2::NatGateway",
      "Properties" : {
        "AllocationId" : { "Fn::GetAtt" : ["EIP-NATGateway", "AllocationId"]},
        "SubnetId" : { "Ref" : "public-az1"},
        "Tags" : [ {"Key" : "Application", "Value" : { "Ref" : "AWS::StackId"} } ]
      }
    },

    "EIP-NATGateway" : {
      "Type" : "AWS::EC2::EIP",
      "Properties" : {
        "Domain" : "VPC"
      }
    },

    "RouteTableNAT" : {
      "Type" : "AWS::EC2::RouteTable",
      "Properties" : {
        "VpcId" : {"Ref" : "VPC"},
        "Tags" : [ {"Key" : "Application", "Value" : { "Ref" : "AWS::StackId"} } ]
      }
    },
    
    "RouteNAT" : {
      "Type" : "AWS::EC2::Route",
      "Properties" : {
        "RouteTableId" : { "Ref" : "RouteTableNAT" },
        "DestinationCidrBlock" : "0.0.0.0/0",
        "NatGatewayId" : { "Ref" : "NATGateway" }
      }
    },

    "SubnetRouteTableAssociationNAT1" : {
      "Type" : "AWS::EC2::SubnetRouteTableAssociation",
      "Properties" : {
        "SubnetId" : { "Ref" : "private-az1" },
        "RouteTableId" : { "Ref" : "RouteTableNAT" }
      }
    },

    "SubnetRouteTableAssociationNAT2" : {
      "Type" : "AWS::EC2::SubnetRouteTableAssociation",
      "Properties" : {
        "SubnetId" : { "Ref" : "private-az2" },
        "RouteTableId" : { "Ref" : "RouteTableIGW" }
      }
    },

    "ALB-Varnish" : {
      "Type" : "AWS::ElasticLoadBalancingV2::LoadBalancer",
      "Properties" : {
        "Scheme" : "internet-facing",
        "Subnets" : [ { "Ref" : "public-az1"}, { "Ref" : "public-az2" } ],
        "SecurityGroups" : [ { "Ref" : "SG-ALB-Varnish" } ]
      }
    },

    "ALB-VarnishListener" : {
      "Type" : "AWS::ElasticLoadBalancingV2::Listener",
      "Properties" : {
        "DefaultActions" : [{
          "Type" : "forward",
          "TargetGroupArn" : { "Ref" : "TG-ALB-Varnish" }
        }],
        "LoadBalancerArn" : { "Ref" : "ALB-Varnish" },
        "Port" : "80",
        "Protocol" : "HTTP"
      }
    },

    "TG-ALB-Varnish" : {
      "Type" : "AWS::ElasticLoadBalancingV2::TargetGroup",
      "Properties" : {
        "HealthCheckIntervalSeconds" : 90,
        "HealthCheckTimeoutSeconds" : 60,
        "HealthyThresholdCount" : 3,
        "Port" : 80,
        "Protocol" : "HTTP",
        "UnhealthyThresholdCount" : 5,
        "VpcId" : {"Ref" : "VPC"}
      }
    },

    "SG-ALB-Varnish" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "Public ELB Security Group with HTTP access on port 80 from the internet",
        "VpcId" : { "Ref" : "VPC" },
        "SecurityGroupIngress" : [ { "IpProtocol" : "tcp", "FromPort" : "80", "ToPort" : "80", "CidrIp" : "0.0.0.0/0"} ],
        "SecurityGroupEgress" : [ { "IpProtocol" : "tcp", "FromPort" : "80", "ToPort" : "80", "CidrIp" : "0.0.0.0/0"} ]
      }
    },
  
    "ALB-Magento" : {
      "Type" : "AWS::ElasticLoadBalancingV2::LoadBalancer",
      "Properties" : {
        "Scheme" : "internet-facing",
        "Subnets" : [ { "Ref" : "public-az1"}, { "Ref" : "public-az2" } ],
        "SecurityGroups" : [ { "Ref" : "SG-ALB-Magento" } ]
      }
    },

    "ALB-MagentoListener" : {
      "Type" : "AWS::ElasticLoadBalancingV2::Listener",
      "Properties" : {
        "DefaultActions" : [{
          "Type" : "forward",
          "TargetGroupArn" : { "Ref" : "TG-ALB-Magento" }
        }],
        "LoadBalancerArn" : { "Ref" : "ALB-Magento" },
        "Port" : "80",
        "Protocol" : "HTTP"
      }
    },

    "TG-ALB-Magento" : {
      "Type" : "AWS::ElasticLoadBalancingV2::TargetGroup",
      "Properties" : {
        "HealthCheckIntervalSeconds" : 90,
        "HealthCheckTimeoutSeconds" : 60,
        "HealthyThresholdCount" : 3,
        "Port" : 80,
        "Protocol" : "HTTP",
        "UnhealthyThresholdCount" : 5,
        "VpcId" : {"Ref" : "VPC"}
      }
    },

    "SG-ALB-Magento" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "Public ELB Security Group with HTTP access on port 80 from the internet",
        "VpcId" : { "Ref" : "VPC" },
        "SecurityGroupIngress" : [ { "IpProtocol" : "tcp", "FromPort" : "80", "ToPort" : "80", "CidrIp" : "0.0.0.0/0"} ],
        "SecurityGroupEgress" : [ { "IpProtocol" : "tcp", "FromPort" : "80", "ToPort" : "80", "CidrIp" : "0.0.0.0/0"} ]
      }
    }, 

    "ALB-Workers" : {
      "Type" : "AWS::ElasticLoadBalancingV2::LoadBalancer",
      "Properties" : {
        "Scheme" : "internal",
        "Subnets" : [ { "Ref" : "private-az1"}, { "Ref" : "private-az2" } ],
        "SecurityGroups" : [ { "Ref" : "SG-ALB-Workers" } ]
      }
    },

    "ALB-WorkersListener" : {
      "Type" : "AWS::ElasticLoadBalancingV2::Listener",
      "Properties" : {
        "DefaultActions" : [{
          "Type" : "forward",
          "TargetGroupArn" : { "Ref" : "TG-ALB-Workers" }
        }],
        "LoadBalancerArn" : { "Ref" : "ALB-Workers" },
        "Port" : "80",
        "Protocol" : "HTTP"
      }
    },

    "TG-ALB-Workers" : {
      "Type" : "AWS::ElasticLoadBalancingV2::TargetGroup",
      "Properties" : {
        "HealthCheckIntervalSeconds" : 90,
        "HealthCheckTimeoutSeconds" : 60,
        "HealthyThresholdCount" : 3,
        "Port" : 80,
        "Protocol" : "HTTP",
        "UnhealthyThresholdCount" : 5,
        "VpcId" : {"Ref" : "VPC"}
      }
    },

    "SG-ALB-Workers" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "Private ELB Security Group with HTTP access on port 80",
        "VpcId" : { "Ref" : "VPC" },
        "SecurityGroupIngress" : [ { "IpProtocol" : "tcp", "FromPort" : "80", "ToPort" : "80", "CidrIp" : "0.0.0.0/0"} ],
        "SecurityGroupEgress" : [ { "IpProtocol" : "tcp", "FromPort" : "80", "ToPort" : "80", "CidrIp" : "0.0.0.0/0"} ]
      }
    },

    "ASG-Varnish-Group" : {
      "Type" : "AWS::AutoScaling::AutoScalingGroup",
      "Properties" : {
        "AvailabilityZones" : [ { "Ref" : "eu-west-1a"}, { "Ref" : "eu-west-1b" } ], 
        "LaunchConfigurationName" : { "Ref" : "LC-Varnish" },
        "MinSize" : "2",
        "MaxSize" : "3",
        "LoadBalancerNames" : [ { "Ref" : "ALB-Varnish" } ]
      }
    },

    "ASP-Varnish-ScaleUpPolicy" : {
      "Type" : "AWS::AutoScaling::ScalingPolicy",
      "Properties" : {
        "AdjustmentType" : "ChangeInCapacity",
        "AutoScalingGroupName" : { "Ref" : "ASG-Varnish-Group" },
        "Cooldown" : "60",
        "ScalingAdjustment" : "1"
      }
    },
    "ASP-Varnish-ScaleDownPolicy" : {
      "Type" : "AWS::AutoScaling::ScalingPolicy",
      "Properties" : {
        "AdjustmentType" : "ChangeInCapacity",
        "AutoScalingGroupName" : { "Ref" : "ASG-Varnish-Group" },
        "Cooldown" : "60",
        "ScalingAdjustment" : "-1"
      }
    },

    "CLW-Varnish-CPUAlarmHigh": {
      "Type": "AWS::CloudWatch::Alarm",
      "Properties": {
         "AlarmDescription": "Scale-up if CPU > 90% for 10 minutes",
         "MetricName": "CPUUtilization",
         "Namespace": "AWS/EC2",
         "Statistic": "Average",
         "Period": "300",
         "EvaluationPeriods": "2",
         "Threshold": "90",
         "AlarmActions": [ { "Ref": "ASP-Varnish-ScaleUpPolicy" } ],
         "Dimensions": [
           {
             "Name": "AutoScalingGroupName",
             "Value": { "Ref": "ASG-Varnish-Group" }
           }
         ],
         "ComparisonOperator": "GreaterThanThreshold"
       }
     },
     "CLW-Varnish-CPUAlarmLow": {
      "Type": "AWS::CloudWatch::Alarm",
      "Properties": {
         "AlarmDescription": "Scale-down if CPU < 70% for 10 minutes",
         "MetricName": "CPUUtilization",
         "Namespace": "AWS/EC2",
         "Statistic": "Average",
         "Period": "300",
         "EvaluationPeriods": "2",
         "Threshold": "70",
         "AlarmActions": [ { "Ref": "ASP-Varnish-ScaleDownPolicy" } ],
         "Dimensions": [
           {
             "Name": "AutoScalingGroupName",
             "Value": { "Ref": "ASG-Varnish-Group" }
           }
         ],
         "ComparisonOperator": "LessThanThreshold"
       }
     },

    "LC-Varnish" : {
      "Type" : "AWS::AutoScaling::LaunchConfiguration",
      "Properties" : {
        "KeyName" : { "Ref" : "KeyName" },
        "ImageId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "AMI" ]},
        "SecurityGroups" : [ { "Ref" : "SG-ALB-Varnish" } ],
        "InstanceType" : { "Ref" : "t2.large" },
        "BlockDeviceMappings" : [
          {
            "DeviceName" : "/dev/sdm",
            "Ebs" : { "VolumeSize" : "100", "DeleteOnTermination" : "true"}
          }
       ]
      }
    },

    
    "ASG-Workers-Group" : {
      "Type" : "AWS::AutoScaling::AutoScalingGroup",
      "Properties" : {
        "AvailabilityZones" : [ { "Ref" : "eu-west-1a"}, { "Ref" : "eu-west-1b" } ], 
        "LaunchConfigurationName" : { "Ref" : "LC-Workers" },
        "MinSize" : "2",
        "MaxSize" : "3",
        "LoadBalancerNames" : [ { "Ref" : "ALB-Workers" } ]
      }
    },

    "ASP-Workers-ScaleUpPolicy" : {
      "Type" : "AWS::AutoScaling::ScalingPolicy",
      "Properties" : {
        "AdjustmentType" : "ChangeInCapacity",
        "AutoScalingGroupName" : { "Ref" : "ASG-Workers-Group" },
        "Cooldown" : "60",
        "ScalingAdjustment" : "1"
      }
    },
    "ASP-Workers-ScaleDownPolicy" : {
      "Type" : "AWS::AutoScaling::ScalingPolicy",
      "Properties" : {
        "AdjustmentType" : "ChangeInCapacity",
        "AutoScalingGroupName" : { "Ref" : "ASG-Workers-Group" },
        "Cooldown" : "60",
        "ScalingAdjustment" : "-1"
      }
    },

    "CLW-Workers-CPUAlarmHigh": {
      "Type": "AWS::CloudWatch::Alarm",
      "Properties": {
         "AlarmDescription": "Scale-up if CPU > 90% for 10 minutes",
         "MetricName": "CPUUtilization",
         "Namespace": "AWS/EC2",
         "Statistic": "Average",
         "Period": "300",
         "EvaluationPeriods": "2",
         "Threshold": "90",
         "AlarmActions": [ { "Ref": "ASP-Workers-ScaleUpPolicy" } ],
         "Dimensions": [
           {
             "Name": "AutoScalingGroupName",
             "Value": { "Ref": "ASG-Workers-Group" }
           }
         ],
         "ComparisonOperator": "GreaterThanThreshold"
       }
     },
     "CLW-Workers-CPUAlarmLow": {
      "Type": "AWS::CloudWatch::Alarm",
      "Properties": {
         "AlarmDescription": "Scale-down if CPU < 70% for 10 minutes",
         "MetricName": "CPUUtilization",
         "Namespace": "AWS/EC2",
         "Statistic": "Average",
         "Period": "300",
         "EvaluationPeriods": "2",
         "Threshold": "70",
         "AlarmActions": [ { "Ref": "ASP-Workers-ScaleDownPolicy" } ],
         "Dimensions": [
           {
             "Name": "AutoScalingGroupName",
             "Value": { "Ref": "ASG-Workers-Group" }
           }
         ],
         "ComparisonOperator": "LessThanThreshold"
       }
     },

    "LC-Workers" : {
      "Type" : "AWS::AutoScaling::LaunchConfiguration",
      "Properties" : {
        "KeyName" : { "Ref" : "KeyName" },
        "ImageId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "AMI" ]},
        "SecurityGroups" : [ { "Ref" : "SG-ALB-Workers" } ],
        "InstanceType" : { "Ref" : "t2.large" },
        "BlockDeviceMappings" : [
          {
            "DeviceName" : "/dev/sdm",
            "Ebs" : { "VolumeSize" : "100", "DeleteOnTermination" : "true"}
          }
       ]
      }
    },

    "Ec2-MagentoAdmin" : {
      "Type" : "AWS::EC2::Instance",
      "Properties" : {
        "ImageId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "AMI" ]},
        "SubnetId" : { "Ref" : "private-az1" },
        "KeyName" : { "Ref" : "KeyName" },
        "InstanceType" : { "Ref" : "t2.large" }
      }    
    },

    
    "Ec2-MagentoAdmin-Volumen": {
       "Type": "AWS::EC2::Volume",
       "Properties": {
          "AvailabilityZone": { "Fn::GetAtt": [ "Ec2-MagentoAdmin", "AvailabilityZone"] },
          "Encrypted": "false",
          "Size": "1000",
          "Tags": [
                {
                    "Key": "-",
                    "Value": "-"
                }
            ],
          "VolumeType" : "gp2"
       }
    },

    "EBS-MagentoAdmin-MountPoint" : {
      "Type" : "AWS::EC2::VolumeAttachment",
      "Condition" : "AttachVolume",
      "Properties" : {
        "Device" : { "Ref" : "/dev/sdh" },
        "InstanceId" : { "Ref" : "Ec2-MagentoAdmin" },
        "VolumeId" : { "Ref" : "Ec2-MagentoAdmin-Volumen" }
      }
    },

    
    "Ec2-Bastion" : {
      "Type" : "AWS::EC2::Instance",
      "Properties" : {
        "ImageId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "AMI" ]},
        "SubnetId" : { "Ref" : "public-az1" },
        "KeyName" : { "Ref" : "KeyName" },
        "InstanceType" : { "Ref" : "t2.large" }
      }    
    },

    
    "Ec2-Bastion-Volumen": {
       "Type": "AWS::EC2::Volume",
       "Properties": {
          "AvailabilityZone": { "Fn::GetAtt": [ "Ec2-Bastion", "AvailabilityZone"] },
          "Encrypted": "false",
          "Size": "100",
          "Tags": [
                {
                    "Key": "-",
                    "Value": "-"
                }
            ],
          "VolumeType" : "gp2"
       }
    },

    "EBS-Bastion-MountPoint" : {
      "Type" : "AWS::EC2::VolumeAttachment",
      "Condition" : "AttachVolume",
      "Properties" : {
        "Device" : { "Ref" : "/dev/sdh" },
        "InstanceId" : { "Ref" : "Ec2-Bastion" },
        "VolumeId" : { "Ref" : "Ec2-Bastion-Volumen" }
      }
    },

    "EIP-Bastion": {
      "Type": "AWS::EC2::EIP",
      "Properties": {
        "Domain": "vpc",
        "InstanceId": "Ec2-Bastion"
      }
    },

    "Ec2-RMS-Threat-and-Log-Manager" : {
      "Type" : "AWS::EC2::Instance",
      "Properties" : {
        "ImageId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "AMI" ]},
        "SubnetId" : { "Ref" : "public-az1" },
        "KeyName" : { "Ref" : "KeyName" },
        "InstanceType" : { "Ref" : "c4.large" }
      }    
    },

    "Ec2-RMS-Threat-and-Log-Manager-Volumen": {
      "Type": "AWS::EC2::Volume",
      "Properties": {
         "AvailabilityZone": { "Fn::GetAtt": [ "Ec2-RMS-Threat-and-Log-Manager", "AvailabilityZone"] },
         "Encrypted": "false",
         "Size": "60",
         "Tags": [
               {
                   "Key": "-",
                   "Value": "-"
               }
           ],
         "VolumeType" : "gp2"
      }
   },

   "EBS-RMS-Threat-and-Log-Manager-MountPoint" : {
     "Type" : "AWS::EC2::VolumeAttachment",
     "Condition" : "AttachVolume",
     "Properties" : {
       "Device" : { "Ref" : "/dev/sdh" },
       "InstanceId" : { "Ref" : "Ec2-RMS-Threat-and-Log-Manager" },
       "VolumeId" : { "Ref" : "Ec2-RMS-Threat-and-Log-Manager-Volumen" }
     }
   },

   "EIP-RMS-Threat-and-Log-Manager": {
    "Type": "AWS::EC2::EIP",
    "Properties": {
      "Domain": "vpc",
      "InstanceId": "Ec2-RMS-Threat-and-Log-Manager"
    }
  },

   "Ec2-NFS" : {
    "Type" : "AWS::EC2::Instance",
    "Properties" : {
      "ImageId" : { "Fn::FindInMap" : [ "RegionMap", { "Ref" : "AWS::Region" }, "AMI" ]},
      "SubnetId" : { "Ref" : "private-az1" },
      "KeyName" : { "Ref" : "KeyName" },
      "InstanceType" : { "Ref" : "m4.large" }
    }    
   },

  
   "Ec2-NFS-Volumen": {
     "Type": "AWS::EC2::Volume",
     "Properties": {
        "AvailabilityZone": { "Fn::GetAtt": [ "Ec2-NFS", "AvailabilityZone"] },
        "Encrypted": "false",
        "Size": "100",
        "Tags": [
              {
                  "Key": "-",
                  "Value": "-"
              }
          ],
        "VolumeType" : "gp2"
     }
   },

   "EBS-NFS-MountPoint" : {
    "Type" : "AWS::EC2::VolumeAttachment",
    "Condition" : "AttachVolume",
    "Properties" : {
      "Device" : { "Ref" : "/dev/sdh" },
      "InstanceId" : { "Ref" : "Ec2-NFS" },
      "VolumeId" : { "Ref" : "Ec2-NFS-Volumen" }
      }
   }
  }
}